#Листинг 9.50: Миграция для добавления логического атрибута admin к пользователям. db/migrate/[timestamp]_add_admin_to_users.rb 
class AddAdminToUsers < ActiveRecord::Migration
  def change
        add_column :users, :admin, :boolean, default: false

  end
end
