#Oct 07
#Листинг 9.39: Rake-задача для заполнения базы данных образцами пользователей. db/seeds.rb 
#9.51 :12.14 namefioshow-stud-master
#Листинг 12.14: Добавление взаимоотношений читаемый/читатель к образцам данных. db/seeds.rb
#Листинг 9.51: Код заполнителя образцов данных для создания административного пользователя. db/seeds.rb
#Листинг 12.14: Добавление взаимоотношений читаемый/читатель к образцам данных. db/seeds.rb

# Пользователи
User.create!(name:  "Example User",
             email: "example@railstutorial.org",
             password:              "foobar",
             password_confirmation: "foobar",
             admin:     true,
             activated: true,
             activated_at: Time.zone.now)

99.times do |n|
  name  = Faker::Name.name
  email = "example-#{n+1}@railstutorial.org"
  password = "password"
  User.create!(name: name,
              email: email,
              password:              password,
              password_confirmation: password,
              activated: true,
              activated_at: Time.zone.now)
end

# Микросообщения
users = User.order(:created_at).take(6)
50.times do
  content = Faker::Lorem.sentence(5)
  users.each { |user| user.microposts.create!(content: content) }
end

# Взаимоотношения
users = User.all
user  = users.first
following = users[2..50]
followers = users[3..40]
following.each { |followed| user.follow(followed) }
followers.each { |follower| follower.follow(user) }

