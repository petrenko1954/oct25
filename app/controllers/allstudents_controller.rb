class AllstudentsController < ApplicationController

def index
    @allstudents = Student.all
    @users = Student.paginate(page: params[:page])

  end
end
