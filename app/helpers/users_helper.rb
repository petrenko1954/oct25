# Листинг 7.8: Представление, показывающее пользователя с именем и граватаром. app/#views/users/show.html.erbSept 01 Листинг 7.9: Определение вспомогательного метода #gravatar_for. app/helpers/users_helper.rb 
module UsersHelper

  # Возвращает граватар для данного пользователя
  def gravatar_for(user)
    gravatar_id = Digest::MD5::hexdigest(user.email.downcase)
    gravatar_url = "https://secure.gravatar.com/avatar/#{gravatar_id}"
    image_tag(gravatar_url, alt: user.name, class: "gravatar")
  end
end

